CC = 

ifeq ($(shell uname -s), Linux)
CC = gcc
else ifeq ($(shell uname -s), FreeBSD)
CC = clang
endif

all: 
	$(CC) server.c -o server

sub: 
	cd commands && $(MAKE) -C ./

clean: subclean
	rm -f *.out
	rm -f *.core
	rm -f *.txt
	rm -f server

subclean:
	cd commands && $(MAKE) clean -C ./


