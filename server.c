#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/socket.h>
#include <netinet/tcp.h>
#include <sys/uio.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdarg.h>

#define QLEN 32
#define BUFSIZE 15000

#ifndef NDEBUG
#  define debug_print(msg) stderr_printf msg
#else
#  define debug_print(msg) (void)0
#endif

struct numPipe {
    int counter;        // when counter == 0, need to be the stdin of command
    int pipe[2];        // store the pipe
};

/* Debug function */
void stderr_printf(const char *fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
}


int createSocket( int port ){

    int sockfd;
    int sockval;
    struct protoent *ppe;
    struct sockaddr_in sin;

    memset(&sin, 0, sizeof(sin));
    sin.sin_family = AF_INET;
    sin.sin_addr.s_addr = INADDR_ANY;
    sin.sin_port = htons((u_short)port);

    ppe = getprotobyname("tcp");

    // Create socket
    sockfd = socket( PF_INET, SOCK_STREAM, ppe->p_proto );
    if ( sockfd < 0 ) {
        perror("Cannot create socket");
    }

    sockval = 1;
    if( setsockopt( sockfd, SOL_SOCKET, SO_REUSEADDR, (void *)&sockval, sizeof(sockval) ) == -1 ) {
        perror("Cannot set socket option");
    }

    setsockopt(sockfd, IPPROTO_TCP, TCP_NODELAY, (char *) &sockval, sizeof(sockval));

    // Bind socket to port
    if ( bind( sockfd, (struct sockaddr *)&sin, sizeof(sin)) < 0 ){
        perror("Cannot bind socket to target port");
    }

    // Listen on target port
    if ( listen( sockfd, QLEN ) < 0 ) {
        perror("Cannot listen on target port");
    }

    return sockfd;

}

/* 
 * Split command to array of string with null terminal at the end
 */
char **splitCmd( char *command ) {

    int cnt=0;
    char *tmp;
    char **p;

    p = calloc(sizeof(char *) , 20);

    tmp = strtok( command, " \r\n");
    p[cnt] = malloc( sizeof(char) * strlen(tmp) + 1);
    memcpy( p[cnt++], tmp, strlen(tmp)+1 );
    while( ( tmp = strtok( NULL, " \r\n") ) != NULL ) {
        p[cnt] = malloc( sizeof(char) * strlen(tmp) + 1);
        memcpy( p[cnt++], tmp, strlen(tmp)+1 );
    }

    //p[cnt++] = calloc( sizeof(char), 1 );

    return p;
}

int sendPipeContent2user( int fd , int sockfd ) {

    int len;
    char buf[BUFSIZE];

    //TODO: Current assume the output is smaller than BUFSIZE, or will cause error.
    len = read( fd, buf, BUFSIZE );
    debug_print(("[send] Send to user\n"));
    //printf("***************\n");
    //printf("%s\n",buf);
    //printf("***************\n");
    write( sockfd, buf, len );

    return 0;

}

int sendPipeContent2file( int fd , char *file  ) {

    int len, f;
    char buf[BUFSIZE];

    //TODO: Current assume the output is smaller than BUFSIZE, or will cause error.
    len = read( fd, buf, BUFSIZE );
    debug_print(("[send] Send to file\n"));
    //printf("***************\n");
    //printf("%s\n",buf);
    //printf("***************\n");
    f = open( file,  O_RDWR | O_TRUNC | O_CREAT , 0666 );
    write( f, buf, len );

    return 0;

}

int sendPipeContent2pipe( int src , int dst  ) {

    int len;
    char buf[BUFSIZE];

    //TODO: Current assume the output is smaller than BUFSIZE, or will cause error.
    len = read( src, buf, BUFSIZE );
    debug_print(("[send] Send to pipe\n"));
    //printf("***************\n");
    //printf("%s\n",buf);
    //printf("***************\n");
    write( dst, buf, len );

    return 0;

}

int removeArg( char **str, int num, int len ) {

    debug_print(("[removeArg] Remove arg = '%s', position = %d\n", str[num], num));

    // Free the argument
    free( str[num] );

    // Move the argument behind
    for( int i = num; i < len-1; i++ ) {
        str[i] = str[i+1];
    }

    str[len-1] = NULL;

    return len-1;

}

/* Select a numeric pipe to store the command's output */
int selectPipe( int *len, struct numPipe *np ) {

    int target = -1;


    for( int j=0; j<(*len); j++ ) {
        debug_print(("[debug][selectPipe] np[%d].counter = %d\n", j, (np+j)->counter));
        if( (np+j)->counter <= 0 ) {
            target = j;
            break;
        }
    }

    if( target == -1 ) {
        *len += 1;
        target = *len - 1;
    }

    debug_print(("[debug][selectPipe] current plen = %d\n", *len));
    return target;
}

/*
 * Using fork to execute the command
 * Arguments:
 */
void execCmd( char **cmd, int input, int output, int error, int read_out, int read_err ) {

    int pid;
    if( (pid = fork()) == 0 ) {

        /* In child process, we need to close 'read_out', 'read_err'
         * and after replace the standard I/O , we close 'input', 'output', 'error'
         * p.s. dup2() function use arg1 to replace arg2's file number
         *      so in this case, 'input' will be the new standard input
         *                       'output' will be the new standard output
         *                        and 'error' will be the new standard error
         *      for the rest of the program                                   */

        debug_print(("[debug][execCmd] child process: pid = %d\n", getpid() ));
        if( input != STDIN_FILENO )   dup2( input, STDIN_FILENO );
        if( output != STDOUT_FILENO ) dup2( output, STDOUT_FILENO );
        if( error != STDERR_FILENO )  dup2( error, STDERR_FILENO );

        if( read_out != 0 ) close( read_out );
        if( read_err != 0 ) close( read_err );

        if( input != STDIN_FILENO )   close( input );
        if( output != STDOUT_FILENO ) close( output );
        if( error != STDERR_FILENO )  close( error );

        if( execvp( cmd[0], cmd ) == -1 ) {
            fprintf( stderr, "Unknown command: [%s].\n", cmd[0] );
        }

        exit(0);

    } else {

        /* In parent process, we need to close 'input', 'output', 'error' */
        debug_print(("[debug][execCmd] parent process: pid = %d\n", getpid() ));
        if( input != STDIN_FILENO ) close( input );
        if( output != STDOUT_FILENO ) close( output );
        if( error != STDERR_FILENO ) close( error );

        /* Wait the child process */
        waitpid( pid, NULL, 0 );

    }

}

int processingCmd(char *cmds, struct numPipe np[], int *plen, int sock) {

    int i;
    char **cmd;
    char *envwarn = "Command need correct arguments.\n";
    int aggrp[2], errp[2], tmpp[2];

    char *p, *fname;
    char tmp_str[100];
    int input, output, error, read_out, read_err;
    int fuserpipe, tuserpipe;
    int len,counter, arg, t, notleave, witherr, withouterr, curp, frompipe, tosock, tofile;

    debug_print(("[debug] Command : %s\n", cmds));

    cmd = splitCmd(cmds);

    // Count the arg num
    arg = 0;
    for( int j=0; j<20; j++ ) {
        if( cmd[j] == NULL )
            break;
        else
            arg++;
    }

    // Debug phase
    for( int j=0; j<arg; j++ ) {
        debug_print(("[debug] cmd[%d] = %s\n", j, cmd[j]));
    }


    // special commands
    if ( strncmp("printenv", cmd[0], 8) == 0 && strlen(cmd[0]) == 8 ) {
        if( arg != 2 ) {
            write( sock, envwarn , strlen(envwarn) );
        } else {
            memset( tmp_str, 0, 100 );
            sprintf( tmp_str, "%s=%s\n", cmd[1], getenv(cmd[1]) );
            //write( sock, getenv(cmd[1]), strlen(getenv(cmd[1])) );
            write( sock, tmp_str, strlen(tmp_str) );
        }
        return 0;
    } else if ( strncmp("setenv", cmd[0], 6) == 0 && strlen(cmd[0]) == 6 ) {
        if( arg != 3 ) {
            write( sock, envwarn , strlen(envwarn) );
        } else {
            setenv( cmd[1], cmd[2], 1 );
            debug_print(("[debug][processingCmd] %s = %s\n", cmd[1], cmd[2]));
        }
        return 0;
    } else if ( strncmp("exit", cmd[0], 4) == 0 ) {// && strlen(cmd[0]) == 5 ) {
        close( sock );
        //shutdown( sock, SHUT_RDWR );
        return 1;
    } else {

        /* normal commands */

        // Create aggregate pipe and error pipe
        if( pipe(aggrp) < 0 ) {
            perror("Pipe error");
        }

        if( pipe(errp) < 0 ) {
            perror("Pipe error");
        }

        /* Debug */
        debug_print(("[debug][processingCmd] current arg = %d\n", arg));
        debug_print(("[debug][processingCmd] current plen = %d\n", *plen));

        int seq;

        /* Clear the counter */
        fuserpipe = tuserpipe = frompipe = witherr = withouterr = tosock = tofile = 0;
        input = output = error = read_out = read_err = 0;
        p = NULL;

        /* Default : input from stdin */
        input = STDIN_FILENO;

        /* Check if input from numeric pipe */
        
        for( int j=0; j<(*plen); j++ ) {
            np[j].counter--;
            debug_print(("[debug][processingCmd] np[%d].counter = %d\n", j, np[j].counter));
            if( np[j].counter == 0 ) {
                debug_print(("[debug][processingCmd] np[%d].counter == %d, get the output.\n" ,j ,np[j]));
                sendPipeContent2pipe( np[j].pipe[1], aggrp[0] );
                close( np[j].pipe[1] );

                frompipe = 1;
            }
        }

        if ( frompipe ) {
            debug_print(("[debug][processingCmd] get input from numeric pipe\n"));
            // Close till all pipe has send it data to me
            close( aggrp[0] );
            input = aggrp[1];
        } else {
            // Close the unuse pipe prevent someting wrong
            close( aggrp[0] );
            close( aggrp[1] );
        }

        /* Check if output 'stdout & stderr' to numeric pipe */
        if( cmd[arg-1][0] == '!' ) {
            // get the numeric pipe number and remove the argument from cmd
            witherr = 1;
            p = &cmd[arg-1][1];
            seq = atoi(p);
            arg = removeArg( cmd, arg-1, arg );
        }

        /* Check if output 'stdout' to numeric pipe */
        if( cmd[arg-1][0] == '|' ) {
            debug_print(("[debug][processingCmd] command need to pipe output.\n"));
            // get the numeric pipe number and remove the argument from cmd
            withouterr = 1;
            p = &cmd[arg-1][1];
            seq = atoi(p);
            arg = removeArg( cmd, arg-1, arg );
            debug_print(("[debug][processingCmd] setting output to next %d command.\n", seq));
        }

        /* Check if output 'stdout' to file */
        for( int j = arg-1; j >= 0; j-- ) {
            if( strncmp( cmd[j], ">", 1 ) == 0 && strlen( cmd[j] ) == 1 ) {
                // Get the file name
                // Remove cmd pointer after '>' and save the file name
                tofile = 1;
                fname = strdup(cmd[j+1]);
                arg = removeArg( cmd, j+1, arg );
                arg = removeArg( cmd, j, arg );
                break;
            }
        }


        if( witherr ) {

            debug_print(("[debug][processingCmd] send error msg to numeric pipe and output to socket.\n"));

            // Select a numeric pipe to output
            curp = selectPipe( plen, np );
            np[curp].counter = seq;

            // Create pipe
            if( pipe(np[curp].pipe) < 0 ) {
                perror("Pipe error");
            }

            // Setting output
            output = errp[0];
            error = np[curp].pipe[0];
            read_out = errp[1];
            read_err = np[curp].pipe[1];

            // Execute the command
            execCmd( cmd, input, output, error, read_out, read_err );

            // Post processing : send output to user socket
            sendPipeContent2user( read_out, sock );
            close( read_out );

        } else if( withouterr ) {

            debug_print(("[debug][processingCmd] send error msg to user and pipe output to numeric pipe.\n"));

            // Select a numeric pipe to output
            curp = selectPipe( plen, np );
            np[curp].counter = seq;

            // Create pipe
            if( pipe(np[curp].pipe) < 0 ) {
                perror("Pipe error");
            }

            // Setting output
            output = np[curp].pipe[0];
            error = errp[0];
            read_out = np[curp].pipe[1];
            read_err = errp[1];

            // Execute the command
            execCmd( cmd, input, output, error, read_out, read_err );

            // Post processing : send the error to user sock
            sendPipeContent2user( read_err, sock );

        } else if( tofile ) {

            debug_print(("[debug][processingCmd] send error msg to user and send output to file '%s'.\n", fname));

            // Create a tmp pipe to store the output
            if( pipe(tmpp) < 0 ) {
                perror("Pipe error");
            }

            // Setting output
            output = tmpp[0];
            error = errp[0];
            read_out = tmpp[1];
            read_err = errp[1];

            // Execute the command
            execCmd( cmd, input, output, error, read_out, read_err );

            // Post processing : send output to file
            sendPipeContent2file( read_out, fname );
            free( fname );      // prevent memory leak
            close( read_out );

            // Post processing : send error to user socket
            sendPipeContent2user( read_err, sock );
            close( read_err );

        } else {

            debug_print(("[debug][processingCmd] send error msg and output to user socket.\n"));

            // Setting output
            output = errp[0];
            error = errp[0];
            read_out = errp[1];
            read_err = errp[1];

            // Cause the output and error send to the same pipe, we should close the errp, but we can use it to receive the command output
            // close( errp[0] );
            // close( errp[1] );

            // Execute the command
            execCmd( cmd, input, output, error, read_out, read_err );
            close( aggrp[0] );

            // Post processing : send output to user socket
            sendPipeContent2user( read_out, sock );
            close( read_out );

        }

        // Free cmd
        for( int j=0; j<arg; j++ ){
            free( cmd[j] );
            cmd[j] = NULL;
        }

        return 0;
    }
}

int sockShell( int sock ) {

    pid_t pid;
    int input, output, error, read_out, read_err;
    int fuserpipe, tuserpipe;
    int len,counter, arg, t, leave, witherr, plen, curp, frompipe, tosock, tofile;
    //int pp[2], pp2[2], errp[2];
    char *p, *q, *pipe_num_end, *fname, **cmd, **tmp;
    char *greeting = "****************************************\n** Welcome to the information server. **\n****************************************\n";

    char buf[BUFSIZE];
    char cmds[50];
    struct numPipe np[3000];

    setenv("PATH", "bin:.", 1);

    // Clear buffer and generate PIPE
    plen = 0;                                   // for numeric pipe
    leave = 0;                               // Condition to break the while
    memset( buf, 0, BUFSIZE);

    // Initial the numPipe's counter
    for( int j=0; j<1000; j++ ) {
        np[j].counter = 0;
    }


    // Send greeting message.
    write( sock, greeting, strlen(greeting) );

    while ( !leave ) {
        write( sock, "% ", 2 );               // Send the shell prompt
        len = read( sock, buf, BUFSIZE );       // Read from socket
        debug_print(("[sockShell] Command : %s\n", buf));

        /* Parse the command one by one and execute it */
        counter = 0;
        p = buf;

        while( p < &buf[len-1] ) {
            memset( cmds, 0, 50 );
            q = p;
            // Find the longest sub command
            while( *q != '|' && q != &buf[len-1] ) { q++; }

            if ( *q == '|' ) {
                // Get the string till the pipe num end
                if( *(q+1) == ' ' || *(q+1) == '\n' ) {
                    // Pipe to next command, equal to '|1'
                    strncpy( cmds, p, q-p+1 );
                    cmds[q-p+1] = '1';
                    cmds[q-p+2] = '\0';
                    q+=1;
                } else {
                    // Pipe to next 'n' command
                    q+=1;
                    while( *q != ' '  && *q != '\n' ) q++;
                    strncpy( cmds, p, q-p );
                    cmds[q-p+1] = '\0';
                }

                leave = processingCmd( cmds, np, &plen, sock );
                debug_print(("[sockShell] intermidiate command\n"));

            } else {
                // Last sub command
                p = strtok( p, "\r\n" );
                strncpy( cmds, p, strlen(p) );
                leave = processingCmd( cmds, np, &plen, sock );

                // Move q to the end of buffer
                q = &buf[len-1];
                debug_print(("[sockShell] last command\n"));
            }

            p = q+1;
        }


        printf("Helo~\n");

        // Validate output
        //for ( int i=0; i<counter; i++ ){
        //    printf("cmds[%d] = %s\n", i, cmds[i]);
        //}

        // Initial all the pipe to zero
        // We have two normal pipe and one error pipe
        //pp[0] = pp[1] = pp2[0] = pp2[1] = errp[0] = errp[1] = 0;


        /* Clear the buffer */
        //what if len > bufsize
        memset( buf, 0, len+1);



    }

    return 0;
}

int main() {

    int pid;
    int msock, ssock;                   // Server socket and session socket
    struct sockaddr_in fsin;            // Client addr.
    unsigned int alen;                  // Client addr. length
    int fd;

    // Server socket listen at 8888 port
    msock = createSocket( 8888 );

    while(1) {

        ssock = accept( msock, (struct sockaddr* )&fsin, &alen );
        if ( ssock < 0 ) {
            perror("Cannot accept socket");
        }

        if ( ( pid = fork() ) == 0 ) {
            close(msock);
            // Write the greeting message and start interactive shell
            sockShell( ssock );
            return 0;
        } else {
            close(ssock);
        }


    }

}
